package alexa

import (
	"gitlab.com/dasjott/alexa-sdk-go/dialog"
)

// AppID is the ID of the corresponding skill
var AppID string

// Handlers are intent functions to be called by name
var Handlers IntentHandlers

// LocaleStrings are all localized strings
var LocaleStrings Localisation

// BeforeHandler can be set with a function to implement any checking before every intent.
// It returns true for going on with the actual intent or false to skip.
// Remember to implement a appropriate message to the user on skipping!
var BeforeHandler func(*Context)

// Handle is the function you handle over to the lambda.start
var Handle = func(req *dialog.EchoRequest) (*dialog.EchoResponse, error) {
	// if !req.VerifyTimestamp() {
	// 	return "", errors.New("invalid timestamp")
	// }
	if AppID != "" && !req.VerifyAppID(AppID) {
		panic("invalid app id")
	}

	var translation Translation
	if LocaleStrings != nil {
		if lang, exists := LocaleStrings[req.Request.Locale]; exists {
			translation = lang
		} else {
			panic("language " + req.Request.Locale + " not implemented")
			// translation = make(Translation)
		}
	}

	c := start(req, Handlers, translation)
	return c.getResult()
}

// IntentHandler function for the handler
type IntentHandler func(*Context)

// IntentHandlers for collecting the handler functions
type IntentHandlers map[string]IntentHandler

// Add adds a handler afterwards.
func (h IntentHandlers) Add(name string, handler IntentHandler) {
	h[name] = handler
}

// MultiHandler if you need more than one handler for an intent
func MultiHandler(handlers ...IntentHandler) IntentHandler {
	return func(c *Context) {
		count := len(handlers)
		for i := 0; i < count && !c.abort; i++ {
			(handlers[i])(c)
		}
	}
}
