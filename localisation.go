package alexa

import (
	"fmt"
	"reflect"
	"strings"
)

// Localisation maps locale codes to according Translations
type Localisation map[string]Translation

// Translation is a set of translated strings. The value can be either a string or a string array
type Translation map[string]interface{}

// R is a shortcut for map[string]interface{}, while the value must be int (any), float (any) or string
type R map[string]interface{}

// GetString gets a string from the value according to the given key
func (tr Translation) GetString(key string) string {
	if val, exists := tr[key]; exists {
		switch val.(type) {
		case string:
			return val.(string)
		case []string:
			arr := val.([]string)
			if count := len(arr); count > 0 {
				return arr[random.Intn(len(arr))]
			}
		}
	}
	return ""
}

// GetArray gets an array from the value according to the given key
func (tr Translation) GetArray(key string) []string {
	if val, exists := tr[key]; exists {
		switch val.(type) {
		case string:
			str := val.(string)
			return []string{str}
		case []string:
			return val.([]string)
		}
	}
	return []string{}
}

// GetStringAndReplace gets a translated string and replaces given keys with given values.
// Place key in {brackets} to be replaced here!
func (tr Translation) GetStringAndReplace(key string, replace R) string {
	str := tr.GetString(key)
	for k, v := range replace {
		str = strings.Replace(str, "{"+k+"}", fmt.Sprintf("%v", v), -1)
	}
	return str
}

// GetStringWithVariables gets a translated string, where all placeholders are filled with the values from the given struct.
// Place key in {brackets} to be replaced here! As tag name use alexa.
func (tr Translation) GetStringWithVariables(key string, data interface{}) string {
	str := tr.GetString(key)

	setFields(&str, data, "")

	return str
}

func setFields(str *string, data interface{}, prefix string) {
	t, v := reflect.TypeOf(data), reflect.ValueOf(data)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	for i := 0; i < v.NumField(); i++ {
		tf, vf := t.Field(i), v.Field(i)

		name := tf.Name
		if tag := tf.Tag.Get("alexa"); tag != "" {
			name = tag
		}
		if prefix != "" {
			name = prefix + "." + name
		}

		if vf.Kind() == reflect.Struct || (vf.Kind() == reflect.Ptr && vf.Elem().Kind() == reflect.Struct) {
			setFields(str, vf.Interface(), name)
		} else {
			var value string
			if tf.Type.String() == "string" {
				value = vf.String()
			} else if strings.HasPrefix(tf.Type.String(), "int") {
				value = fmt.Sprintf("%d", vf.Int())
			} else if strings.HasPrefix(tf.Type.String(), "float") {
				value = fmt.Sprintf("%f", vf.Float())
			}
			*str = strings.Replace(*str, "{"+name+"}", value, -1)
		}
	}
}
