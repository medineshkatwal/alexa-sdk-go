package alexa

import "gitlab.com/dasjott/alexa-sdk-go/dialog"

const (
	ConfirmationStatusNone      = "NONE"
	ConfirmationStatusDenied    = "DENIED"
	ConfirmationStatusConfirmed = "CONFIRMED"
)

// Slot is a simple representation of the slot object from the echo request
type Slot struct {
	// ID is the ID of that slot value
	ID string
	// Value is the value of slot, not the actual spoken value
	Value string
	// Spoken is the actual value, spoken by the user
	Spoken string
	// ConfirmationStatus is the status of the confirmation of this slot
	ConfirmationStatus string
	// Match is true, if the actual speech is a match to one of this slots values or its synonyms
	Match bool
}

// Empty determines whether this slot is already filled
func (s *Slot) Empty() bool {
	return s.Spoken == ""
}

func slotFromEchoSlot(es *dialog.EchoSlot) *Slot {
	var resolution dialog.NameID
	var match bool

	if es.Resolutions != nil && len(es.Resolutions.ResolutionsPerAuthority) > 0 {
		match = es.Resolutions.ResolutionsPerAuthority[0].Status.Code == "ER_SUCCESS_MATCH"
		if len(es.Resolutions.ResolutionsPerAuthority[0].Values) > 0 {
			resolution = es.Resolutions.ResolutionsPerAuthority[0].Values[0].Value
		}
	}
	// if resolution.Name == "" {
	// 	resolution.Name = es.Value
	// }
	return &Slot{
		ID:                 resolution.ID,
		Value:              resolution.Name,
		Spoken:             es.Value,
		ConfirmationStatus: es.ConfirmationStatus,
		Match:              match,
	}
}
